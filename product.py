class Product:

    #Each product should contain sku, name and price
    def __init__(self, sku, name, price):
        self.sku = sku
        self.name = name
        self.price = price

    def get_price(self):
        return self.price

    def get_sku(self):
        return self.sku
