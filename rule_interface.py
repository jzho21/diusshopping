from abc import ABC, abstractmethod

'''
To force all rule classes with this interface to have its own constructor and
the way to calculate the promotion total
'''
class RuleInterface(ABC):
    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def get_total(self):
        pass
