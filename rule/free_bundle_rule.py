from rule_interface import RuleInterface

'''
Applying basic rule interface into the class
'''


class FreeBundleRule(RuleInterface):
    def __init__(self, shopping_cart, eligible_product, free_bundle_product):
        self.eligible_product = eligible_product
        self.shopping_cart = shopping_cart
        self.free_bundle_product = free_bundle_product

    def get_total(self):
        total_eligible_items = self.shopping_cart.sku_qty_in_the_cart(self.eligible_product.get_sku())

        # Mark the item as paid
        self.shopping_cart.mark_paid_item(self.eligible_product.get_sku(), total_eligible_items)
        self.shopping_cart.mark_paid_item(self.free_bundle_product.get_sku(), total_eligible_items)

        # Mark free bundle item as paid
        return total_eligible_items * self.eligible_product.get_price()
