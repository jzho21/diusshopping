from rule_interface import RuleInterface


class MultiBuyRule(RuleInterface):
    def __init__(self, shopping_cart, eligible_product, min_qty_required, discount_price):
        self.eligible_product = eligible_product
        self.shopping_cart = shopping_cart
        self.min_qty_required = min_qty_required
        self.discount_price = discount_price

    def get_total(self):
        total_eligible_items = self.shopping_cart.sku_qty_in_the_cart(self.eligible_product.get_sku())

        # Mark the item as paid
        self.shopping_cart.mark_paid_item(self.eligible_product.get_sku(), total_eligible_items)

        if total_eligible_items > self.min_qty_required:
            return total_eligible_items * self.discount_price

        return total_eligible_items * self.eligible_product.get_price()
