from rule_interface import RuleInterface


class BuyXForY(RuleInterface):
    def __init__(self, shopping_cart, eligible_product, purchase_qty, pay_qty):
        self.eligible_product = eligible_product
        self.purchase_qty = purchase_qty
        self.pay_qty = pay_qty
        self.shopping_cart = shopping_cart

    '''
    Calculate total for promotion buy x only pay y
    '''
    def get_total(self):
        total_eligible_items = self.shopping_cart.sku_qty_in_the_cart(self.eligible_product.get_sku())

        # Mark the item as paid
        self.shopping_cart.mark_paid_item(self.eligible_product.get_sku(), total_eligible_items)

        if total_eligible_items >= self.purchase_qty:
            return total_eligible_items // self.purchase_qty * self.pay_qty * self.eligible_product.get_price() + \
                   total_eligible_items % self.purchase_qty * self.eligible_product.get_price()

        return total_eligible_items * self.eligible_product.get_price()