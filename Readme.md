## Programming Language ##
- Python 3 ( The reason I chose python is relatively simpler to present Object Oriented Programming 
concept than javascript)

## Shopping cart rule explaination ##
- Shopping cart rule must contain two functions, one is constructor another is the way it calculates
the promotion total.

- After a shopping cart rule has been created, it can simply inject back to shopping cart instance to
enable it.

- The parameters of shopping cart rule in theory can be loaded from database and very flexible to
be modified.

## How to run ##
- There is no fancy interface due to time limit. So to run the app please type `python3 app.py`

## Unit test ##
- The code has been fully cover by unit test. Run `python3 -m unitest`

## Modifying shopping cart ##
- Open `app.py`
- Putting `shopping_cart.add_item(apple_tv)` or `shopping_cart.add_item(ipad)`
or `shopping_cart.add_item(macbook)` or `shopping_cart.add_item(vga_adapter)` after
comoment line `#Adding items into shopping cart`