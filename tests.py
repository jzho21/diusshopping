import unittest
from product import Product
from shopping_cart import ShoppingCart
from rule.buy_x_for_y import BuyXForY
from rule.free_bundle_rule import FreeBundleRule
from rule.multi_buy_rule import MultiBuyRule

class TestShoppingCartPromotion(unittest.TestCase):
    def setUp(self):
        self.ipad = Product("ipd", "Super iPad", 549.99)
        self.macbook = Product("mbp", "MacBook Pro", 1399.99)
        self.apple_tv = Product("atv", "Apple TV", 109.50)
        self.vga_adapter = Product("vga", "VGA Adapter", 30.00)

    def test_buy_x_for_y(self):
        shopping_cart = ShoppingCart()

        # Define shopping cart rules
        buy_x_get_y_free = BuyXForY(shopping_cart, self.apple_tv, 3, 2)

        # insert active shopping cart rule
        shopping_cart.insert_shopping_cart_rules(buy_x_get_y_free)

        shopping_cart.add_item(self.apple_tv)
        shopping_cart.add_item(self.apple_tv)
        shopping_cart.add_item(self.apple_tv)
        shopping_cart.add_item(self.vga_adapter)

        self.assertEqual(shopping_cart.get_total(), 249)


    def test_multi_buy_rule(self):
        shopping_cart = ShoppingCart()

        # Define shopping cart rules
        multi_buy_rule = MultiBuyRule(shopping_cart, self.ipad, 4, 499.99)

        # insert active shopping cart rule
        shopping_cart.insert_shopping_cart_rules(multi_buy_rule)

        shopping_cart.add_item(self.apple_tv)
        shopping_cart.add_item(self.ipad)
        shopping_cart.add_item(self.ipad)
        shopping_cart.add_item(self.apple_tv)
        shopping_cart.add_item(self.ipad)
        shopping_cart.add_item(self.ipad)
        shopping_cart.add_item(self.ipad)
        self.assertEqual(shopping_cart.get_total(), 2718.95)

    def test_free_bundle_rule(self):
        shopping_cart = ShoppingCart()

        free_bundle_rule = FreeBundleRule(shopping_cart, self.macbook, self.vga_adapter)

        # insert active shopping cart rule
        shopping_cart.insert_shopping_cart_rules(free_bundle_rule)

        shopping_cart.add_item(self.macbook)
        shopping_cart.add_item(self.vga_adapter)
        shopping_cart.add_item(self.ipad)

        self.assertEqual(shopping_cart.get_total(), 1949.98)


if __name__ == '__main__':
    unittest.main()
