class ShoppingCart:
    def __init__(self):
        self.items = []
        self.rules = []
        self.total = 0

    def insert_shopping_cart_rules(self, rule):
        self.rules.append(rule)

    def add_item(self, product):
        self.items.append(product)

    '''
    Mark the paid items
    :param sku
    :param qty_remove_counter indicates how many matched products should be marked as paid in shopping cart.
    '''
    def mark_paid_item(self, sku, qty_remove_counter):
        counter = 0

        # Clone the shopping cart to help modify original list
        clone_shopping_cart_items = self.items.copy()

        if qty_remove_counter == 0:
            pass

        for item in clone_shopping_cart_items:
            if item.get_sku() == sku and counter <= qty_remove_counter:
                self.items.remove(item)
                counter = counter + 1

    '''
    check qty for a product in the shopping cart
    :param sku
    '''
    def sku_qty_in_the_cart(self, sku):
        qty = 0
        for item in self.items:
            if item.get_sku() == sku:
                qty = qty + 1

        return qty

    def get_unpaid_item(self):
        return self.items

    '''
    Total for the shopping cart, with all promotions applied.
    '''
    def get_total(self):
        # Apply all shopping cart rules first
        for rule in self.rules:
            self.total = self.total + rule.get_total()

        # Add the total of unpaid items
        for item in self.items:
            self.total = self.total + item.get_price()

        return self.total
