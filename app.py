# Setup the sample data and run the app

from product import Product
from shopping_cart import ShoppingCart
from rule.buy_x_for_y import BuyXForY
from rule.free_bundle_rule import FreeBundleRule
from rule.multi_buy_rule import MultiBuyRule

# Initial shopping cart
shopping_cart = ShoppingCart()

# Load products into memory
ipad = Product("ipd", "Super iPad", 549.99)
macbook = Product("mbp", "MacBook Pro", 1399.99)
apple_tv = Product("atv", "Apple TV", 109.50)
vga_adapter = Product("vga", "VGA Adapter", 30.00)

# Define shopping cart rules
buy_x_get_y_free = BuyXForY(shopping_cart, apple_tv, 3, 2)
multi_buy_rule = MultiBuyRule(shopping_cart, ipad, 4, 499.99)
free_bundle_rule = FreeBundleRule(shopping_cart, macbook, vga_adapter)

# insert active shopping cart rules
shopping_cart.insert_shopping_cart_rules(buy_x_get_y_free)
shopping_cart.insert_shopping_cart_rules(multi_buy_rule)
shopping_cart.insert_shopping_cart_rules(free_bundle_rule)

# Adding items into shopping cart
shopping_cart.add_item(apple_tv)
shopping_cart.add_item(ipad)
shopping_cart.add_item(ipad)
shopping_cart.add_item(apple_tv)
shopping_cart.add_item(ipad)
shopping_cart.add_item(ipad)
shopping_cart.add_item(ipad)

print("Total cost after promotion applied: $%s" % shopping_cart.get_total())
